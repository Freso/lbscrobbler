# LBScrobbler: Scrobble from ListenBrainz to scrobbling platforms
# Copyright © 2019 Frederik “Freso” S. Olesen <https://freso.dk/>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <https://www.gnu.org/licenses/>.
"""Scrobble listens from ListenBrainz to Last.fm, Libre.fm, etc.

LBScrobbler is a utility to scrobble listens from ListenBrainz
to other scrobbling platforms of your choice (e.g., Last.fm, Libre.fm,
etc.) to keep your listening history across profiles synchronised.
"""

from pkg_resources import get_distribution, DistributionNotFound
try:
    __version__ = get_distribution(__name__).version
except DistributionNotFound:
    # not installed as package; fallback to get version directly from git
    from setuptools_scm import get_version
    __version__ = get_version()
