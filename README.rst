LBScrobbler
===========

Utility to scrobble listens from ListenBrainz_ to a scrobbling platform
of your choice (e.g., Last.fm, Libre.fm, etc.) to keep your listening
history across profiles synchronised.

.. _ListenBrainz: https://listenbrainz.org/

Motivation
----------

ListenBrainz is a new player on the field for recording users’
listening histories, and it even includes the ability to import your
listen history (“scrobbles”) from Last.fm.
This feature encourages users who want to “back up” their listening
history to multiple providers will continue to have Last.fm as their
‘primary’ listening target.

This script makes it possible to import a user’s listening history from
ListenBrainz into other scrobble‐based services (such as Last.fm and
Libre.fm). This in turn should make it viable to use ListenBrainz as
one’s primary source for their listening history record, utilising the
increased richness of the ListenBrainz listening protocol over the
older scrobbling protocol.

Hopefully this will eventually get included into ListenBrainz proper
(see LB-425_) which may then make this utility obsolete.

.. _LB-425: https://tickets.metabrainz.org/browse/LB-425

Usage
-----

LBScrobbler has two primary modes of function: oneoff or as a daemon.

oneoff mode
^^^^^^^^^^^

In this mode, LBScrobbler will read the configuration file, convert
listens to scrobbles, and exit. This can be used e.g., with a cron job.

Command::

    lbscrobbler

daemon mode
^^^^^^^^^^^

In this mode, LBScrobbler’s process will fork to the background and
continuously poll ListenBrainz for new listens (incl. “playing now” and
convert those to scrobbles.

Command::

    lbscrobblerd

systemd unit files
^^^^^^^^^^^^^^^^^^

LBScrobbler comes with systemd_ user unit files supporting both modes
of operation.

If you wish to use the oneoff mode in a given frequency [#timer]_,
use::

    systemctl --user enable --now lbscrobbler.timer

If you wish to use the daemon mode, use::

    systemctl --user enable --now lbscrobblerd.service

.. _systemd: https://freedesktop.org/wiki/Software/systemd/
.. [#timer] Defaults to once per hour. See `systemd’s documentation`_
   for how to cleanly change this.
.. _systemd’s documentation:
   https://www.freedesktop.org/software/systemd/man/

Configuration
-------------

Before running LBScrobbler you will need to configure it.
The configuration file is read from
``$XDG_CONFIG_HOME/lbscrobbler/lbscrobbler.toml``,
or, if that doesn’t exist (e.g., if ``$XDG_CONFIG_HOME`` is unset), from
``$HOME/.config/lbscrobbler/lbscrobbler.toml``.

See this example configuration for possible options:

Example configuration file
^^^^^^^^^^^^^^^^^^^^^^^^^^

..
   TODO: Replace with TOML lexer, once PR is accepted and released:
   https://bitbucket.org/birkenfeld/pygments-main/pull-requests/807

.. code-block:: ini

   [listenbrainz]
   username = 'EricPraline'
   token = '' # from https://listenbrainz.org/profile/

   [[servers]]
   name = 'Last.fm'
   server = 'https://ws.audioscrobbler.com/'
   api_version = '2.0'
   # Get your API key from https://www.last.fm/api/authentication
   api_key = 'YOUR_API_KEY'

   [[servers]]
   name = 'Libre.fm'
   server = 'https://turtle.libre.fm/'
   api_version = '2.0'
   username = 'LIBREFM_USERNAME'
   password = 'LIBREFM_PASSWORD'


Installation
------------

If you’re on Linux, check if your package repository has this.
If not, it can be installed through ``pip``::

    python3 -m pip install lbscrobbler

Testing
-------

First ensure that you have the dependencies needed to run tests::

    python3 -m pip install lbscrobbler[test]

Now you can run the tests using::

    python3 -m pytest

Contributing
------------

The project is still in its very early stages. Feel free to submit PRs,
but please adhere to code style and respect that not all the project
goals have been fully fleshed out yet, so your patch may not be in line
with the author’s vision.

Code style
^^^^^^^^^^

The program will have a strict adherence to `PEP 8`_, with the
exception of the line length. It is nice to stay under the 72/79
characters per line, but not an absolute necessity. Additionally, all
classes and functions should have docstrings adhering to `PEP 257`_,
using reStructuredText_ markup when plaintext is not expressive enough.

.. _PEP 8: https://www.python.org/dev/peps/pep-0008/
.. _PEP 257: https://www.python.org/dev/peps/pep-0257/
.. _reStructuredText: http://docutils.sourceforge.net/rst.html

Commits
^^^^^^^

Commits going into the master branch should strive to be
`atomic commits`_ with `good commit messages`_.

.. _atomic commits: https://www.freshconsulting.com/atomic-commits/
.. _good commit messages: https://chris.beams.io/posts/git-commit/

Contact
-------

The project is hosted on GitLab, and any bugs, feature requests, etc.
should go there: https://gitlab.com/Freso/lbscrobbler

You can also discuss this project on `Freenode IRC`_ in the `##Freso
channel`_, or alternatively on Discord_.

.. _Freenode IRC: https://freenode.net/
.. _##Freso channel: https://webchat.freenode.net/?channels=##Freso
.. Maybe change to ircs://chat.freenode.net/##Freso at some point.
.. _Discord: https://discord.gg/hU85Grw


Acknowledgments
---------------

This program was made with the support of `my patrons on Patreon`_.
Thank you so much for supporting my work with open data and free and
open source software!

.. TODO: List individual patrons here…

.. _my patrons on Patreon: https://www.patreon.com/Freso

License
-------

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor,
Boston, MA  02110-1301, USA.
